package uz.pdp.codingbat.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.pdp.codingbat.entity.User;
import uz.pdp.codingbat.repository.UserRepository;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{
    final UserRepository userRepository;

    public ResultMessage add(User user)
    {
        userRepository.save(user);
        return null;
    }
    public User show(Integer id)
    {
        Optional<User> byId = userRepository.findById(id);
        return byId.orElse(null);
    }

    public List<User> showAll()
    {
        return userRepository.userlist();
    }
    public ResultMessage edit(Integer id,User user)
    {
        Optional<User> byId = userRepository.findById(id);
        if(byId.isPresent())
        {
            User user1 = byId.get();
            if(user1.getIsDeleted())
            {
                return new ResultMessage(false,"user deleted");
            }
            user1.setPassword(user.getPassword());
            return new ResultMessage(true,"edited");
        }
        return new ResultMessage(false,"not found");
    }
    public void delete(Integer id)
    {
        Optional<User> byId = userRepository.findById(id);
        if(byId.isPresent()) {
            User user = byId.get();
            user.setIsDeleted(true);
            return;
        }
        return;
    }
}
