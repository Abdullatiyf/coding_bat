package uz.pdp.codingbat.service;

import uz.pdp.codingbat.entity.User;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

public interface UserService {
    ResultMessage add(User user);
    User show(Integer id);
    List<User> showAll();
    ResultMessage edit(Integer id,User country);
    void delete(Integer id);
}
