package uz.pdp.codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.codingbat.entity.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Integer> {
    @Query(nativeQuery = true,value = "select * from users where isDeleted = false")
    public List<User> userlist();
}
