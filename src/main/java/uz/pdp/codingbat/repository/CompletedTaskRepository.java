package uz.pdp.codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.codingbat.entity.CompletedTasks;

public interface CompletedTaskRepository extends JpaRepository<CompletedTasks,Integer> {
}
