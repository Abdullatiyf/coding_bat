package uz.pdp.codingbat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.codingbat.entity.User;
import uz.pdp.codingbat.service.UserService;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserControllerImpl implements UserController{
    final UserService userService;
    @Override
    public ResultMessage add(User user) {
        return userService.add(user);
    }

    @Override
    public void edit(Integer id, User user) {
        userService.edit(id,user);
    }

    @Override
    public List<User> showAll() {
        return userService.showAll();
    }

    @Override
    public User show(Integer id) {
        return userService.show(id);
    }

    @Override
    public void delete(Integer id) {
        userService.delete(id);
    }
}
