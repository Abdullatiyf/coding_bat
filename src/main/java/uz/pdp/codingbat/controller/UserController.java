package uz.pdp.codingbat.controller;

import org.springframework.web.bind.annotation.*;
import uz.pdp.codingbat.entity.User;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

@RequestMapping(path = "/user")
public interface UserController {
    @PostMapping
    ResultMessage add(@RequestBody User user);

    @PutMapping("/{id}")
    void edit(@PathVariable Integer id, @RequestBody User user);

    @GetMapping()
    List<User> showAll();

    @GetMapping("/{id}")
    User show(@PathVariable Integer id);

    @DeleteMapping("/{id}")
    void delete(@PathVariable Integer id);
}
