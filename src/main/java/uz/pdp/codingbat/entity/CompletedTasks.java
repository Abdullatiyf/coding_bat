package uz.pdp.codingbat.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompletedTasks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private User users;
    @ManyToOne
    private Task task;
    private Boolean isCompleted;
    private String answerText;
    private Integer testCaseCount;
    private Integer successTestCaseCount;
    private Boolean isDeleted;
    private LocalDate date;
}
